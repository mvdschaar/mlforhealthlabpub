### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/ac_tpc](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/ac_tpc). Please visit the new repository.

-----------------------

-----------------------

# AC-TPC
Title: "Temporal Phenotyping using Deep Predicting Clustering of Disease Progression"

Authors: Changhee Lee, Mihaela van der Schaar

- Reference: C. Lee, M. van der Schaar, "Temporal Phenotyping using Deep Predicting Clustering of Disease Progression," International Conference on Machine Learning (ICML), 2020
- Paper: the link will be available soon
- Supplementary: the link will be available soon

