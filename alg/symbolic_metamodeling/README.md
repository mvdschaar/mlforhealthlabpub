### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/symbolic_metamodeling](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/symbolic_metamodeling). Please visit the new repository.

-----------------------

-----------------------

# Symbolic-Metamodeling

Code for the NeurIPS 2019 paper "Demystifying Black-box Models with Symbolic Metamodels".
