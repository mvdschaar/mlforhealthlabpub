### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/ganite](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/ganite). Please visit the new repository.

-----------------------

-----------------------

# Ganite

[GANITE](https://openreview.net/forum?id=ByKWUeWA-) a GAN based algorithm for estimating individualized treatment effects

## Usage (TBD)

```
    # run test script
    python3 test_ganite.py
```
