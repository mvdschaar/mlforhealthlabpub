### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/grv](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/grv). Please visit the new repository.

-----------------------

-----------------------

# Gradient Regularized V-Learning for Dynamic Treatment Regimes

This is a python implementation of the algorithms in the paper "Gradient Regularized V-Learning for Dynamic Treatment Regimes" published in NeurIPS 2020. The goal of this algorithm is to evaluate treatment rules or find the optimal treatment rules from observational data.

# Dependencies
Python 3.6 or later and Tensorflow 1.9.0, see [`requirements.txt`](./requirements.txt).

# Examples
`python train_grvb.py`  
`python train_grvs.py` 