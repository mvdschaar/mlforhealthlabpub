### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/discriminative-jackknife](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/discriminative-jackknife). Please visit the new repository.

-----------------------

-----------------------

# discriminative-jackknife
Codebase for "Discriminative Jackknife: Quantifying Uncertainty in Deep Learning via Higher-Order Influence Functions", ICML 2020.

If you use our code in your research, please cite:
```sh
@inproceedings{DJ2020,
	author = {Ahmed M. Alaa, Mihaela van der Schaar},
	title = {Discriminative Jackknife: Quantifying Uncertainty in Deep Learning via Higher-Order Influence Functions},
	booktitle = {International Conference on Machine Learning},
	year = {2020}
}
```
