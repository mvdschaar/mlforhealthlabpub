### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/knockoffgan](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/knockoffgan). Please visit the new repository.

-----------------------

-----------------------

# Knockoff GAN

Written by Jinsung Yoon
[Knockoff Gan](https://openreview.net/forum?id=ByeZ5jC5YQ): "*generating knockoffs for feature selection using generative adversarial networks*". 
Contact: jsyoon0823@g.ucla.edu

## Usage

See also tutorial

```
Rscript knockoffgan.r -i <csv> --target <label>  --fdr <false discovery rate> -o <json file> --stat <rf|glm>
```





