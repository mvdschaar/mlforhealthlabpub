### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/rnn-blockwise-jackknife](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/rnn-blockwise-jackknife). Please visit the new repository.

-----------------------

-----------------------

# rnn-blockwise-jackknife
Codebase for "Frequentist Uncertainty in Recurrent Neural Networks via Blockwise Influence Functions", ICML 2020.

If you use our code in your research, please cite:
```sh
@inproceedings{bjrnn2020,
	author = {Ahmed M. Alaa, Mihaela van der Schaar},
	title = {Frequentist Uncertainty in Recurrent Neural Networks via Blockwise Influence Functions},
	booktitle = {International Conference on Machine Learning},
	year = {2020}
}
```
