### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlab/tree/main/alg/transductive_dropout](https://github.com/vanderschaarlab/mlforhealthlab/tree/main/alg/transductive_dropout). Please visit the new repository.

-----------------------

-----------------------

# Tranductive Dropout

Example code for the paper:

## [Unlabelled Data Improves Bayesian Uncertainty Calibration under Covariate Shift](https://arxiv.org/abs/2006.14988)

### Alex J. Chan, Ahmed M. Alaa, Zhaozhi Qian, and Mihaela van der Schaar

### International Conference on Machine Learning (ICML) 2020



Last Updated: 21 July 2020

Code Author: Alex Chan (ajc340@cam.ac.uk)

An implementation of a transductive dropout network class can be found in models.py and a walkthrough of its use in an example regression problem is provided in tutorial.ipynb

Dependencies:
Autograd, 
matplotlib, 
tqdm

Reference: 
A. J. Chan, A. M. Alaa, Z. Qian, and M. van der Schaar, "Unlabelled Data Improves Bayesian Uncertainty Calibration under Covariate Shift", Proceedings of the 37th International Conference on Machine Learning (ICML), 2020