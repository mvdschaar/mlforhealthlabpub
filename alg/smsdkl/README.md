### **Important**
#### This code has been moved to [https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/smsdkl](https://github.com/vanderschaarlab/mlforhealthlabpub/tree/main/alg/smsdkl). Please visit the new repository.

-----------------------

-----------------------

# Stepwise Model Selection for Sequence Prediction via Deep Kernel Learning

This is a python implementation of the algorithm in the paper "Stepwise Model Selection for Sequence Prediction via Deep Kernel Learning".

# Dependencies
Python 3.6 or later, Tensorflow 1.14, keras 2.2.4

# First steps
To get started, test_smsdkl.py will walk you through the key components in SMS-DKL

# Reference 
The household dataset
https://archive.ics.uci.edu/ml/datasets/individual+household+electric+power+consumption
